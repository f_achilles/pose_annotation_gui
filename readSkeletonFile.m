function [poseStorage,timestamps] = readSkeletonFile(filepath_skel,filepath_depth)
% read a skeleton file and visualize it!
% storage format: cell
makeMovie = false;

% read skeleton file
% filepath = 'C:\Projects\IMUHumanPose\annotationprogram\IM1226_sz07_from1700to1743.txt';
fid = fopen(filepath_skel);
readFrames = 0;
 
while true
    % frame number
    cLine = fgetl(fid);
    if feof(fid)
        break
    end
    readFrames = readFrames+1;
    
    frameIndStorage{readFrames} = sscanf(cLine,'%d');
    timestamps{readFrames} = sscanf(fgetl(fid),'%f');
    % joints
    for i=1:15
        try
            poseStorage{readFrames,i} = sscanf(fgetl(fid),'%f %f %f');
        catch
            poseStorage = poseStorage(1:(end-1),:);
            frameIndStorage = frameIndStorage(1:(end-1));
            timestamps = timestamps(1:(end-1));
            readFrames = readFrames-1;
            break
        end
    end
end
fclose(fid);

connMat = [1 2; 2 3; 3 4; 4 5; 2 6; 6 7; 7 8; 2 9; 9 10; 10 11; 11 12; 9 13; 13 14; 14 15];

% % visualize skeleton (preliminary)
% visFig = figure;
% for idx = 1:readFrames
%     hold on
%     for iJoint = 1:15
%         hJoints(iJoint) = plot3(poseStorage{idx,iJoint}(1),...
%             poseStorage{idx,iJoint}(2),...
%             poseStorage{idx,iJoint}(3));
%     end
%     for iConn = 1:14
%         posBegin = poseStorage{idx,connMat(iConn,1)};
%         posEnd = poseStorage{idx,connMat(iConn,2)};
%         hConn(iConn) = line([posBegin(1) posEnd(1)],[posBegin(2) posEnd(2)],[posBegin(3) posEnd(3)],'color','k');
%     end
%     hold off
%     pause(0.06)
% end


% read corresponding Depth file
% depthFile = 'C:\Projects\SeizureDetection\data\IM1226sz07\20141028142626697#1.kdpt';
depthContainer = playKDPT(filepath_depth);
% irFile = 'C:\Projects\SeizureDetection\data\IM1226sz07\20141028142626697#1.kir';
% irContainer = playKIR(irFile);

% Where the fuck are these values from???
fx = 368.096588;
fy = 368.096588;
cx = 261.696594;
cy = 202.522202;

oldposeStorage = poseStorage;
% convert skeleton points to XYZ
for idx = 1:readFrames
    for iJoint = 1:15
        positionPx = oldposeStorage{idx,iJoint};
        jointDepth = single(depthContainer(round(positionPx(2)),round(positionPx(1)),frameIndStorage{idx}));
        if jointDepth==0
            if idx>1
                jointDepth = poseStorage{idx-1,iJoint}(3);
            else
                warning('depth is zero in the first frame!')
            end
        else
        jointDepth = jointDepth-10*positionPx(3); % MINUS HERE!
        jointDepth = jointDepth*-1;
        end
        positionXYZ = [(jointDepth/fx)*(positionPx(1)-cx),...
            (jointDepth/fy)*(positionPx(2)-cy),...
            jointDepth];
        poseStorage{idx,iJoint} = positionXYZ;
    end
end

% index 7: Left Elbow
% index 8: Left Wrist
% for iJoint = 1:15

%     pos3d = reshape([poseStorage{:,8}],3,[])';
%     figure; plot(pos3d);
%     axis image
%     figure; plot3(pos3d(:,1),pos3d(:,2),pos3d(:,3));
%     axis image vis3d

    % end

% figure;
% pos3d = reshape([poseStorage{1,:}],3,15)';
% plot3(pos3d(:,1),pos3d(:,2),pos3d(:,3),'r*');
% axis image vis3d
% cAxis = axis;
% cAxis([1,3,5]) = cAxis([1,3,5]) - 300;
% cAxis([2,4,6]) = cAxis([2,4,6]) + 300;
% for iFr = 1:size(poseStorage,1)
%     [a,b] = view;
%     pos3d = reshape([poseStorage{iFr,:}],3,15)';
%     plot3(pos3d(:,1),pos3d(:,2),pos3d(:,3),'r*');
%     axis(cAxis)
%     view(a,b)
%     pause
% end

end