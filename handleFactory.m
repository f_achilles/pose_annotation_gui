classdef handleFactory < matlab.mixin.SetGet
	properties
		data
	end
	methods
		function obj = handleFactory(data)
            obj.data = data;
        end
        function obj = set.data(obj,indata)
            obj.data = indata;
        end
        function outdata = get.data(obj)
            outdata = obj.data;
        end
	end
end