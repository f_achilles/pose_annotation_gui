function [acc,accTS] = computeAccFromXYZT(poseStorage,timestamps,joint_ind)
timestamps = [timestamps{:}];
% secTs = timestamps*3600*24;
% for iJ = 1:size(poseStorage,2)
%     pos{iJ} = reshape([poseStorage{:,iJ}],3,[])';
%     vel{iJ} =  bsxfun(@rdivide,...
%                 pos{iJ}(3:end,:) - pos{iJ}(1:(end-2),:),...
%                 (secTs(3:end)-secTs(1:(end-2)))');
%     acc{iJ} =  bsxfun(@rdivide,...
%                 vel{iJ}(3:end,:) - vel{iJ}(1:(end-2),:),...
%                 (secTs(4:(end-1))-secTs(2:(end-3)))');
% end
% accTS = timestamps(3:(end-2))';

Ts = 1/50;
Ts_datenum = Ts/(24*3600);
tsArray_interp_kin = timestamps(1):Ts_datenum:timestamps(end);
tsArray_interp_kin_seconds = tsArray_interp_kin*3600*24;
joint_position_raw = reshape([poseStorage{:,joint_ind}],3,[])';
joint_position_medfilt = medfilt1(joint_position_raw,5,[],1);
joint_position_medfilt_interp50 = interp1(timestamps,joint_position_medfilt,tsArray_interp_kin);
sigma = 10;
gauss_wdw = 101;
gauss_x = linspace(-gauss_wdw/2,gauss_wdw/2,gauss_wdw);
gauss_filter = exp(-gauss_x.^2/(sigma^2));
gauss_filter = gauss_filter/sum(gauss_filter);
xF = conv(joint_position_medfilt_interp50(:,1),gauss_filter,'valid');
yF = conv(joint_position_medfilt_interp50(:,2),gauss_filter,'valid');
zF = conv(joint_position_medfilt_interp50(:,3),gauss_filter,'valid');
joint_position_medfilt_interp50_gauss101Sig20 = [xF,yF,zF]/1000; %conversion to meters
% crop tsArray_interp_kin_seconds to the length that conv(*,*,'valid')
% creates
oldN = numel(tsArray_interp_kin_seconds);
newN = numel(xF);
newStartInd = floor((oldN-newN)/2);
newEndInd = newStartInd+newN-1;
tsArray_interp_kin_seconds = tsArray_interp_kin_seconds(newStartInd:newEndInd);
timestamps = tsArray_interp_kin(newStartInd:newEndInd);
vel = bsxfun(@rdivide,...
                joint_position_medfilt_interp50_gauss101Sig20(3:end,:) - joint_position_medfilt_interp50_gauss101Sig20(1:(end-2),:),...
                (tsArray_interp_kin_seconds(3:end)-tsArray_interp_kin_seconds(1:(end-2)))');
acc = bsxfun(@rdivide,...
                vel(3:end,:) - vel(1:(end-2),:),...
                (tsArray_interp_kin_seconds(4:(end-1))-tsArray_interp_kin_seconds(2:(end-3)))');
accTS = timestamps(3:(end-2))';
% accMag_mPerSecSec = sqrt(acc(:,1).^2+acc(:,2).^2+acc(:,3).^2);
% figure; plot(accTS,accMag_mPerSecSec)

end
