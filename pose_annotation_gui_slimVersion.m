function varargout = pose_annotation_gui_slimVersion(varargin)
% POSE_ANNOTATION_GUI_SLIMVERSION MATLAB code for pose_annotation_gui_slimVersion.fig
%      POSE_ANNOTATION_GUI_SLIMVERSION, by itself, creates a new POSE_ANNOTATION_GUI_SLIMVERSION or raises the existing
%      singleton*.
%
%      H = POSE_ANNOTATION_GUI_SLIMVERSION returns the handle to a new POSE_ANNOTATION_GUI_SLIMVERSION or the handle to
%      the existing singleton*.
%
%      POSE_ANNOTATION_GUI_SLIMVERSION('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in POSE_ANNOTATION_GUI_SLIMVERSION.M with the given input arguments.
%
%      POSE_ANNOTATION_GUI_SLIMVERSION('Property','Value',...) creates a new POSE_ANNOTATION_GUI_SLIMVERSION or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before pose_annotation_gui_slimVersion_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to pose_annotation_gui_slimVersion_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help pose_annotation_gui_slimVersion

% Last Modified by GUIDE v2.5 21-Jan-2017 13:18:50

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @pose_annotation_gui_slimVersion_OpeningFcn, ...
                   'gui_OutputFcn',  @pose_annotation_gui_slimVersion_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before pose_annotation_gui_slimVersion is made visible.
function pose_annotation_gui_slimVersion_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to pose_annotation_gui_slimVersion (see VARARGIN)

% Choose default command line output for pose_annotation_gui_slimVersion
handles.output = hObject;
handles.busy = true;
handles.h_jointInd = handleFactory(0);
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes pose_annotation_gui_slimVersion wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = pose_annotation_gui_slimVersion_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in loadKinectButton.
function loadKinectButton_Callback(hObject, eventdata, handles)
% hObject    handle to loadKinectButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[ir_file, filepath] = uigetfile('*.KIR');
handles.mainFig = pose_annotation_gui_slimVersion;
[IRcontainer,TScontainer] = playKIR(fullfile(filepath,ir_file),true);
% implay(IRcontainer);
handles.data = IRcontainer;
handles.timestamps = TScontainer;

figure(handles.mainFig)
colormap hot;
tmp = handles.data(:,:,1);
tmp(tmp>3000) = 3000;
himage=imagesc(tmp,[0 2^12-1]);
axis image
% himage=imshow(histeq(IRcontainer(:,:,1)));
handles.mainFigAx = handles.axes1;
handles.cImageHandle = himage;
title(handles.mainFigAx, ir_file);
handles.frameInd = 1;
handles.totalframes=size(IRcontainer,3);

set(handles.startbutton,'Enable','on') ;
% set(handles.initframe, 'String',num2str(1));
% set(handles.curframe, 'String',num2str(1));
% set(handles.finalframe, 'String',num2str(handles.totalframes));

guidata( hObject, handles);

% --- Executes on button press in startbutton.
% function startbutton_Callback(hObject, eventdata, handles)
% % hObject    handle to startbutton (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% load initial_skeleton
% for i=1:15
%     h(i) = impoint(gca, position{i,1});
%     resume(h(i))
% end
% handles.hPos=h;
% nframes=size(handles.data,3);
% handles.nextPos=cell(nframes,15);
% handles.depth=zeros(nframes,15);
% set(handles.curframe,'string',num2str(handles.frameInd));
% 
% %saving initial and final frame
% guidata( hObject, handles);
% --- Executes on button press in startbutton.
function startbutton_Callback(hObject, eventdata, handles)
% hObject    handle to startbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.busy = true;
%saving initial and final frame
guidata( hObject, handles);
%initialize joints
load initial_skeleton
handles.circDiam = 10;
figure(handles.mainFig);
% axes(handles.mainFigAx)
%initialize joint-connections
connMat = [1 2; 2 3; 3 4; 4 5; 2 6; 6 7; 7 8; 2 9; 9 10; 10 11; 11 12; 9 13; 13 14; 14 15];
for i=1:14
    posBegin = position{connMat(i,1),1};
    posEnd = position{connMat(i,2),1};
    hConn(i) = line([posBegin(1) posEnd(1)],[posBegin(2) posEnd(2)],'color','k');
end
handles.hConn=hConn;

for i=1:15
%     h(i) = impoint(handles.mainFigAx, position{i,1});
    h(i) = imellipse(handles.mainFigAx,...
        [position{i,1}-[handles.circDiam handles.circDiam]/2 [handles.circDiam handles.circDiam]]);
    h(i).setResizable(false);
    % make left side red, center green
    if any(i==[3 4 5 10 11 12])
        h(i).setColor('red')
    elseif any(i==[1 2 9])
        h(i).setColor('green')
    end
    resume(h(i));
end
handles.hPos=h;

for i=1:15
    % add connection
    addNewPositionCallback(h(i),@(pos)connectJoints(handles,i,connMat,pos));
end
    
nframes=size(handles.data,3);
handles.nextPos=cell(nframes,15);
% handles.depth=zeros(nframes,15);
% set(handles.curframe,'string',num2str(handles.frameInd));
set(handles.initframe,'Enable','on') ;
set(handles.finalframe,'Enable','on') ;

% initialize struct to keep track of each frame
handles.framestatus = struct('was_visited',false,'skeleton',[],'occl_depths',[]);
handles.busy = false;
%saving initial and final frame
guidata( hObject, handles);

function connectJoints(handles, jointIndex, connMat, jointPos)
hConn = handles.hConn;
% hPos = handles.hPos;
% search connection begin change
changeIdx = find(any(connMat(:,1) == jointIndex,2));
if ~isempty(changeIdx)
for idx = changeIdx'
    xdat = get(hConn(idx),'XData');
    ydat = get(hConn(idx),'YData');
    set(hConn(idx),'XData',[jointPos(1)+handles.circDiam/2,xdat(2)])
    set(hConn(idx),'YData',[jointPos(2)+handles.circDiam/2,ydat(2)])
end
end
drawnow
% search connection end change
changeIdx = find(any(connMat(:,2) == jointIndex,2));
if ~isempty(changeIdx)
for idx = changeIdx'
    xdat = get(hConn(idx),'XData');
    ydat = get(hConn(idx),'YData');
    set(hConn(idx),'XData',[xdat(1),jointPos(1)+handles.circDiam/2])
    set(hConn(idx),'YData',[ydat(1),jointPos(2)+handles.circDiam/2])
end
end
drawnow
% store the joint number into some handle here
handles.h_jointInd.set('data',jointIndex);
% handles.latestMovedJointInd = jointIndex;
% handles.busy = false;
% guidata(handles.mainFig, handles);

% figure(pose_annotation_gui) %same result as: set(0,'CurrentFigure',pose_annotation_gui)


% --- Executes on button press in nextFrame.
function nextFrame_Callback(hObject, eventdata, handles)
% hObject    handle to nextFrame (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.busy = true;
guidata(hObject, handles);
if ~isempty(handles.hPos)
    hskeleton=handles.hPos;
    for i=1:15
        tempPos = getPosition(hskeleton(i));
        handles.nextPos{handles.frameInd,i}=tempPos(1:2)+[handles.circDiam handles.circDiam]/2;
    end
end

handles.framestatus(handles.frameInd).was_visited = true;
handles.framestatus(handles.frameInd).skeleton = [handles.nextPos{handles.frameInd,:}];

% load next frame
frameInd = handles.frameInd;
frameInd = frameInd+1;
handles.frameInd = frameInd;
%Button is deactivated if reaching the last frame
if handles.nInitial < frameInd && frameInd < handles.nFinal
    set(handles.previousFrame,'Enable','on')
    set(handles.nextFrame,'Enable','on')
elseif frameInd==handles.nInitial
    set(handles.previousFrame,'Enable','off')
elseif frameInd<handles.nInitial
    return
elseif frameInd==handles.nFinal
    set(handles.nextFrame,'Enable','off')
elseif frameInd>handles.nFinal
    return
end
    
% display next frame
tmp = handles.data(:,:,frameInd);
tmp(tmp>3000) = 3000;
set(handles.cImageHandle,'CData',tmp);
set(handles.curframe,'string',num2str(frameInd));

% if available, show skeleton of the next frame
if numel(handles.framestatus)>=handles.frameInd % prevent indexing non-existent element
if handles.framestatus(handles.frameInd).was_visited
    hskeleton=handles.hPos;
    for i=1:15
        setPosition(hskeleton(i),[handles.nextPos{handles.frameInd,i}-[handles.circDiam handles.circDiam]/2 [handles.circDiam handles.circDiam]]);
    end
end
end
handles.busy = false;
guidata(hObject, handles);


function previousFrame_Callback(hObject, eventdata, handles)
% hObject    handle to previousFrame (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% load prev frame
handles.busy = true;
guidata(hObject, handles);
frameInd = handles.frameInd;
frameInd = frameInd-1;
handles.frameInd = frameInd;
if handles.nInitial < frameInd && frameInd < handles.nFinal
    set(handles.previousFrame,'Enable','on')
    set(handles.nextFrame,'Enable','on')
elseif frameInd==handles.nInitial
    set(handles.previousFrame,'Enable','off')
elseif frameInd<handles.nInitial
    return
elseif frameInd==handles.nFinal
    set(handles.nextFrame,'Enable','off')
elseif frameInd>handles.nFinal
    return
end

set(handles.curframe,'string',num2str(frameInd));
% set(handles.cImageHandle,'CData',histeq(handles.data(:,:,frameInd)));
tmp = handles.data(:,:,frameInd);
tmp(tmp>3000) = 3000;
set(handles.cImageHandle,'CData',tmp);

%initialize joint-connections
connMat = [1 2; 2 3; 3 4; 4 5; 2 6; 6 7; 7 8; 2 9; 9 10; 10 11; 11 12; 9 13; 13 14; 14 15];
if handles.framestatus(handles.frameInd).was_visited
for i=1:14
    posBegin = handles.nextPos{frameInd,connMat(i,1)};
    posEnd = handles.nextPos{frameInd,connMat(i,2)};
    set(handles.hConn(i),'XData',[posBegin(1) posEnd(1)],'YData',[posBegin(2) posEnd(2)])
%     hConn(i) = line([posBegin(1) posEnd(1)],[posBegin(2) posEnd(2)],'color','k');
end
for i=1:15
    setPosition(handles.hPos(i),[handles.nextPos{handles.frameInd,i}-[handles.circDiam handles.circDiam]/2 [handles.circDiam handles.circDiam]]);
end
end
handles.busy = false;
guidata(hObject, handles); 


function initframe_Callback(hObject, eventdata, handles)
% hObject    handle to initframe (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of initframe as text
%        str2double(get(hObject,'String')) returns contents of initframe as a double
handles.busy = true;
guidata(hObject, handles);

text = get(handles.initframe, 'String'); nInitial=str2num(text);

if nInitial >0 && nInitial<handles.totalframes
   handles.frameInd = nInitial;
   handles.nInitial = nInitial;
%    set(handles.cImageHandle,'CData',histeq(handles.data(:,:,handles.frameInd)));
   tmp = handles.data(:,:,handles.frameInd);
   tmp(tmp>3000) = 3000;
   set(handles.cImageHandle,'CData',tmp);
   set(handles.nextFrame,'Enable','on') ;
   set(handles.curframe,'string',num2str(handles.frameInd));
end
handles.busy = false;
guidata( hObject, handles);

function finalframe_Callback(hObject, eventdata, handles)
% hObject    handle to finalframe (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of finalframe as text
%        str2double(get(hObject,'String')) returns contents of finalframe as a double

text = get(handles.finalframe, 'String'); nFinal=str2num(text);
if nFinal > handles.nInitial
    if  nFinal < handles.totalframes
        handles.nFinal=nFinal;
    else
        handles.nFinal=handles.totalframes;
    end
end
handles.busy = false;
set(handles.finalframe, 'String', num2str(handles.nFinal));
guidata( hObject, handles);

% --- Executes on button press in savefile.
function savefile_Callback(hObject, eventdata, handles)
% hObject    handle to savefile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[filename1,filepath1]=uiputfile({'*.txt*','Text Files'},...
    'Save New File');
fileID=fopen(fullfile(filepath1,filename1),'w');
[nrows,ncols] = size(handles.nextPos);
for iFr = handles.nInitial:handles.frameInd
    fprintf(fileID,'%2.0f\n',iFr);
    fprintf(fileID,'%6.10f\n',handles.timestamps(iFr));
    for cols = 1:ncols
        fprintf(fileID,'%2.3f %2.3f %2.3f\n',handles.nextPos{iFr,cols},0);
    end
end
fclose(fileID);


function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function startbutton_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function initframe_CreateFcn(hObject, eventdata, handles)
% hObject    handle to initframe (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function finalframe_CreateFcn(hObject, eventdata, handles)
% hObject    handle to finalframe (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function curframe_Callback(hObject, eventdata, handles)
% hObject    handle to curframe (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of curframe as text
%        str2double(get(hObject,'String')) returns contents of curframe as a double


% --- Executes during object creation, after setting all properties.
function curframe_CreateFcn(hObject, eventdata, handles)
% hObject    handle to curframe (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on key press with focus on figure1 or any of its controls.
function figure1_WindowKeyPressFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.FIGURE)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)

if handles.busy
    return
end

switch eventdata.Key
    case 'w'
        nextFrame_Callback(handles.nextFrame, eventdata, handles)
    case 'q'
        previousFrame_Callback(handles.previousFrame, eventdata, handles)
    case 'rightarrow'
        i=handles.h_jointInd.get.data;
        if i>0
            tempPos = getPosition(handles.hPos(i));
            setPosition(handles.hPos(i),[tempPos(1)+0.5 tempPos(2:4)]);
        guidata(hObject, handles);
        end
    case 'leftarrow'
        i=handles.h_jointInd.get.data;
        if i>0
            tempPos = getPosition(handles.hPos(i));
            setPosition(handles.hPos(i),[tempPos(1)-0.5 tempPos(2:4)]);
        guidata(hObject, handles);
        end
    case 'uparrow'
        i=handles.h_jointInd.get.data;
        if i>0
            tempPos = getPosition(handles.hPos(i));
            setPosition(handles.hPos(i),[tempPos(1) tempPos(2)-0.5 tempPos(3:4)]);
        guidata(hObject, handles);
        end
    case 'downarrow'
        i=handles.h_jointInd.get.data;
        if i>0
            tempPos = getPosition(handles.hPos(i));
            setPosition(handles.hPos(i),[tempPos(1) tempPos(2)+0.5 tempPos(3:4)]);
        guidata(hObject, handles);
        end
end
