% read a skeleton file and visualize it!
% storage format: cell
makeMovie = false;

% read skeleton file
filepath = 'C:\Projects\IMUHumanPose\annotationprogram\IM1226_sz07_from1700to1743.txt';
fid = fopen(filepath);
readFrames = 0;
 
while true
    % frame number
    cLine = fgetl(fid);
    if feof(fid)
        break
    end
    readFrames = readFrames+1;
    
    frameIndStorage{readFrames} = sscanf(cLine,'%d');
    % joints
    for i=1:15
        poseStorage{readFrames,i} = sscanf(fgetl(fid),'%f %f %f');
    end
end
fclose(fid);

connMat = [1 2; 2 3; 3 4; 4 5; 2 6; 6 7; 7 8; 2 9; 9 10; 10 11; 11 12; 9 13; 13 14; 14 15];

% % visualize skeleton (preliminary)
% visFig = figure;
% for idx = 1:readFrames
%     hold on
%     for iJoint = 1:15
%         hJoints(iJoint) = plot3(poseStorage{idx,iJoint}(1),...
%             poseStorage{idx,iJoint}(2),...
%             poseStorage{idx,iJoint}(3));
%     end
%     for iConn = 1:14
%         posBegin = poseStorage{idx,connMat(iConn,1)};
%         posEnd = poseStorage{idx,connMat(iConn,2)};
%         hConn(iConn) = line([posBegin(1) posEnd(1)],[posBegin(2) posEnd(2)],[posBegin(3) posEnd(3)],'color','k');
%     end
%     hold off
%     pause(0.06)
% end


% read corresponding Depth file
depthFile = 'C:\Projects\SeizureDetection\data\IM1226sz07\20141028142626697#1.kdpt';
depthContainer = playKDPT(depthFile);
irFile = 'C:\Projects\SeizureDetection\data\IM1226sz07\20141028142626697#1.kir';
irContainer = playKIR(irFile);

% Where the fuck are these values from???
fx = 368.096588;
fy = 368.096588;
cx = 261.696594;
cy = 202.522202;

oldposeStorage = poseStorage;
% convert skeleton points to XYZ
for idx = 1:readFrames
    for iJoint = 1:15
        positionPx = oldposeStorage{idx,iJoint};
        jointDepth = single(depthContainer(round(positionPx(2)),round(positionPx(1)),frameIndStorage{idx}));
        if jointDepth==0
            if idx>1
                jointDepth = poseStorage{idx-1,iJoint}(3);
            else
                error('depth is zero in the first frame! dammit')
            end
        else
        jointDepth = jointDepth-10*positionPx(3); % MINUS HERE!
        jointDepth = jointDepth*-1;
        end
        positionXYZ = [(jointDepth/fx)*(positionPx(1)-cx),...
            (jointDepth/fy)*(positionPx(2)-cy),...
            jointDepth];
        poseStorage{idx,iJoint} = positionXYZ;
    end
end
%%
if makeMovie
vidH = VideoWriter('IM1226_seizure7_realVideo_withIR.avi');
vidH.FrameRate = 20;
open(vidH);
end
% visualize skeleton (and make Movie)
visFig = figure;
% visFig.Color=[0 0 0.3];
visFig.Color=[1 1 1];
visFig.Position = [800 100 600 600];
[xS,yS,zS] = sphere(20);
partRadius = 40; %mm
xS = xS*partRadius;
yS = yS*partRadius;
zS = zS*partRadius;
pc = zeros(424,512,3);
[XInd,YInd] = meshgrid(1:512,1:424);
for idx = 1:readFrames
% idx = 1
cla
hold on
for iConn = 1:14
    posBegin = poseStorage{idx,connMat(iConn,1)};
    posEnd = poseStorage{idx,connMat(iConn,2)};

    CyHandle(iConn) = Cylinder(posBegin,posEnd,partRadius,30,'b',0,0);

end
for iJoint = 1:15
hJoints(iJoint) = surf(xS+poseStorage{idx,iJoint}(1),...
    yS+poseStorage{idx,iJoint}(2),...
    zS+poseStorage{idx,iJoint}(3),...
    'facecolor','red',...
    'edgecolor','none');
end


% add depth "blanket"
D = -single(depthContainer(:,:,frameIndStorage{idx}));
D(D==0)=NaN;
pc(:,:,3) = D;
pc(:,:,1) = (D/fx).*(XInd-cx);
pc(:,:,2) = (D/fy).*(YInd-cy);

% % write depthmap to PCL file
% savepcd(sprintf('depth_frame%d.pcd',frameIndStorage{idx}), -pc, 'binary')

% % debug
% figure;
% surf(pc(:,:,1),pc(:,:,2),pc(:,:,3),'facealpha',0.9,'edgecolor','none');
% axis vis3d image
% xlabel('X')
% ylabel('Y')
% zlabel('Z')
% % flat x,y-pointcloud
% figure;
% plot3(reshape(pc(:,:,1),1,[]),reshape(pc(:,:,2),1,[]),reshape(pc(:,:,3),1,[])*0,'r*');
% axis vis3d image
% view(2)

% ask user to click on bed-edges and the floor!
% nFloorPts = 6;
% nBedPts = 4;
% figure; imagesc(pc(:,:,3));
% caxis([-2300 -1000])


xPosVec = reshape(pc(:,:,1),1,[]);
yPosVec = reshape(pc(:,:,2),1,[]);
zPosVec = reshape(pc(:,:,3),1,[]);
RMat = rotx(pi*30/180);
RMat = RMat(1:3,1:3);
% rotate pc 30 degrees about x axis
rotatedPC = RMat * [xPosVec;yPosVec;zPosVec];
% xPosVec unchanged
% yPosVec is not used
zPosVec = rotatedPC(3,:);
% figure; plot3(rotatedPC(1,:),rotatedPC(2,:),rotatedPC(3,:),'g*')

% remove all vertices below floor
% floor depth is -2160
goodVertices = true(size(zPosVec));
goodVertices(isnan(zPosVec)) = false;
goodVertices(zPosVec<-2160) = false;

% remove all vertices that have abs(X)>1000
goodVertices(abs(xPosVec)>750) = false;

% delauney triangulation in XY plane
%tic
xPosVec = xPosVec(goodVertices)';
yPosVec = yPosVec(goodVertices)';
DT = delaunayTriangulation(xPosVec,yPosVec);
%toc % 0.8 seconds in full resolution!
% plot
figure; triplot(DT)

yPosVec = rotatedPC(2,goodVertices)';
zPosVec = zPosVec(goodVertices)';

% generate bottom points to close the mesh
hullIDs = DT.convexHull;
hullIDs = hullIDs(1:(end-1));
bottomX = xPosVec(hullIDs)/1000;
bottomY = yPosVec(hullIDs)/1000;
bottomZ = -0.05*ones(size(bottomX));
bottomDT = delaunayTriangulation(bottomX,bottomY);
% make normals point downwards
bottomDT_connList = ...
    [bottomDT.ConnectivityList(:,3) bottomDT.ConnectivityList(:,2) bottomDT.ConnectivityList(:,1)];
maxInd = nnz(goodVertices);
bottomDT_connList = bottomDT_connList+maxInd;
% plot
% figure; triplot(bottomDT)
% connect convex hulls of bottom and top
% (conv. hull numbering follows right-hand scheme)
sides_connList = zeros(numel(hullIDs)*2,3);
for iHull = 1:numel(hullIDs)
    if iHull < numel(hullIDs)
        % upper triangle
        sides_connList(2*iHull-1,:) = [hullIDs(iHull+1) hullIDs(iHull) maxInd+iHull];
        % lower triangle
        sides_connList(2*iHull,:) = [hullIDs(iHull+1) maxInd+iHull maxInd+iHull+1];
    else
        % upper triangle
        sides_connList(2*iHull-1,:) = [hullIDs(1) hullIDs(iHull) maxInd+iHull];
        % lower triangle
        sides_connList(2*iHull,:) = [hullIDs(1) maxInd+iHull maxInd+1];
    end
end

% set proper height and scale to meters
zPosVec = zPosVec+2160;
OBJ.vertices = [...
    [xPosVec yPosVec zPosVec]/1000;
    [bottomX bottomY bottomZ]];
connList = [DT.ConnectivityList;bottomDT_connList;sides_connList];
TR = triangulation(connList,OBJ.vertices);

% % plot
% figure; trisurf(TR,'edgecolor','none','facecolor','interp')
% axis vis3d image
% camlight
% lighting gouraud

OBJ.vertices_normal = TR.vertexNormal();
OBJ.objects(1).type = 'f';
OBJ.objects(1).data.vertices = connList;
OBJ.objects(1).data.normal = connList;
write_wobj(OBJ,sprintf('depth_frame%d.obj',frameIndStorage{idx}));

% % remove annoying connections in surf-plot
% diffZx = abs([diff(D,1,2) zeros(424,1)]);
% diffZy = abs([diff(D,1,1); zeros(1,512)]);
% edgeMask = diffZx>150 | diffZy>150;
% edgeMask = imdilate(edgeMask,ones(3));
% ZforSurfPlot = D;
% ZforSurfPlot(edgeMask)=NaN;
% 
% % IR = (log(double(irContainer(:,:,frameIndStorage{idx}))+1)/log(2^16))*255;
% IR = histeq(double(irContainer(:,:,frameIndStorage{idx}))/2^16);
% IR(edgeMask)=NaN;
% colormap(gray)
% surf(pc(:,:,1),pc(:,:,2),ZforSurfPlot,IR,'facealpha',0.9,'edgecolor','none');
% caxis([0 1.1])
% % surf(pc(:,:,1),pc(:,:,2),ZforSurfPlot,'facecolor',[0.3 0.3 0.3],'facealpha',0.75,'edgecolor','none');
% 
% 
% hold off
% axis equal
% axis([-1000 1000 -1000 1250 -3000 -900])
% axis off vis3d
% % lighting gouraud
% % camlight
% % rotate the objects
% cAxis = gca;
% for iChildren = 1:numel(cAxis.Children)
% rotate(cAxis.Children(iChildren),[1 0 0],30)
% end
% view(0,90-idx/3)
% drawnow
if makeMovie
currFrame = getframe;
writeVideo(vidH,currFrame);
end
% pause(0.03)
end
if makeMovie
close(vidH);
end
% view
%%
% depth camera intrinsic parameters:
% fx 368.096588, fy 368.096588, cx 261.696594, cy 202.522202

% 1) read in KDPT files! #done
% 1.5) make spheres as joints and tubes as limbs #done
% 2) transform joint position to integer, calculate [X,Y,Z] #done
% 3) add depth from file! Visualize! #done
% 4) let one video run! #done
% 5) remove the stupid surf-triangles
% 6) think about camera sweeps --> do the first one just from the side,
% going up, then down again

sz5_matrix = cell2mat(poseStorageSz5);
sz11_matrix = cell2mat(poseStorageSz11);

% plot from i=1:190
numFr = 190;
% plot motion of right knee
motionFig = figure;
motionFig.Color = 'w';

subplot(3,1,1)
hold on
plot((1:numFr)/15,sz5_matrix(1:numFr,10*3+1),'linewidth',5)
plot((1:numFr)/15,sz11_matrix(1:numFr,10*3+1),'linewidth',5)
ylabel('X')

subplot(3,1,2)
hold on
plot((1:numFr)/15,sz5_matrix(1:numFr,10*3+2),'linewidth',5)
plot((1:numFr)/15,sz11_matrix(1:numFr,10*3+2),'linewidth',5)
ylabel('Y')

subplot(3,1,3)
hold on
plot((1:numFr)/15,sz5_matrix(1:numFr,10*3+3),'linewidth',5)
plot((1:numFr)/15,sz11_matrix(1:numFr,10*3+3),'linewidth',5)
xlabel('Zeit in Sekunden')
ylabel('Z')

legend('Seizure 5','Seizure 11')

%%
R=corrcoef(sz5_matrix(1:numFr,:),sz11_matrix(1:numFr,:));

%% distance euclidean
sz5_noMean = bsxfun(@minus,sz5_matrix(1:numFr,:),mean(sz5_matrix(1:numFr,:),1));
sz11_noMean = bsxfun(@minus,sz11_matrix(1:numFr,:),mean(sz11_matrix(1:numFr,:),1));

% diff
difference = sz5_noMean-sz11_noMean;
% square
diffsq = difference.^2;

euclideanD = zeros(1,15);
for i=1:15
    euclideanD(i)= mean(sqrt(sum(diffsq((i-1)*3+1:i*3),2)));
end
meanEuclD = mean(euclideanD)

%%
for iJoint = 1:15
    allJointPositions = reshape([poseStorage{:,iJoint}],3,[]);
    fourierX = fft(allJointPositions(1,:)-mean(allJointPositions(1,:)));
    fourierY = fft(allJointPositions(2,:)-mean(allJointPositions(2,:)));
    fourierZ = fft(allJointPositions(3,:)-mean(allJointPositions(3,:)));
%     figure; plot(1:readFrames,abs(fourierX),'-r',...
%         1:readFrames,abs(fourierY),'-g',...
%         1:readFrames,abs(fourierZ),'-b')
    fourierMean = mean([fourierX;fourierY;fourierZ],1);
    [maxAbs, maxInd] = max(fourierMean(1:floor(readFrames/2)));
    avgFreq(iJoint) = (maxInd-1)*(15/readFrames);
    
    [~, maxIndX] = max(fourierX(1:floor(readFrames/2)));
    avgFreqX(iJoint) = (maxIndX-1)*(15/readFrames);
    [~, maxIndY] = max(fourierY(1:floor(readFrames/2)));
    avgFreqY(iJoint) = (maxIndY-1)*(15/readFrames);
    [~, maxIndZ] = max(fourierZ(1:floor(readFrames/2)));
    avgFreqZ(iJoint) = (maxIndZ-1)*(15/readFrames);
    
end
freqMean = mean([avgFreqX;avgFreqY;avgFreqZ],1);



%% skeleton only (with trace)
paintTrace = false;
makeMovie = false;
if makeMovie
vidH = VideoWriter('seizure11Video_SkeletonTrace.avi');
vidH.FrameRate = 20;
open(vidH);
end
% visualize skeleton (and make Movie)
visFig = figure;
% visFig.Color=[0 0 0.3];
visFig.Color=[1 1 1];
visFig.Position = [800 100 600 600];
[xS,yS,zS] = sphere(20);
partRadius = 40; %mm
xS = xS*partRadius;
yS = yS*partRadius;
zS = zS*partRadius;
pc = zeros(424,512,3);
[XInd,YInd] = meshgrid(1:512,1:424);
for idx = 1:readFrames
% idx = 1
% cla
hold on
if idx > 1, CyHandle.delete; end
for iConn = 1:14
    posBegin = poseStorage{idx,connMat(iConn,1)};
    posEnd = poseStorage{idx,connMat(iConn,2)};
    CyHandle(iConn) = Cylinder(posBegin,posEnd,partRadius/2,30,'b',0,0);
end
if idx > 1, hJoints.delete; end
for iJoint = 1:15
    hJoints(iJoint) = surf(xS+poseStorage{idx,iJoint}(1),...
        yS+poseStorage{idx,iJoint}(2),...
        zS+poseStorage{idx,iJoint}(3),...
        'facecolor','red',...
        'edgecolor','none');
end

if idx == 1
    for iJoint = 1:15
        xOld(iJoint) = poseStorage{idx,iJoint}(1);
        yOld(iJoint) = poseStorage{idx,iJoint}(2);
        zOld(iJoint) = poseStorage{idx,iJoint}(3);
    end
else
    for iJoint = 1:15
        xNew(iJoint) = poseStorage{idx,iJoint}(1);
        yNew(iJoint) = poseStorage{idx,iJoint}(2);
        zNew(iJoint) = poseStorage{idx,iJoint}(3);
    end
    if idx == 2
        lengths = sqrt(sum([[xOld-xNew];[yOld-yNew];[zOld-zNew]].^2,1));
    else
        lengths = lengths + sqrt(sum([[xOld-xNew];[yOld-yNew];[zOld-zNew]].^2,1));
    end
    hLines = line([xOld;xNew],[yOld;yNew],[zOld;zNew]);
    xOld = xNew;
    yOld = yNew;
    zOld = zNew;
end
hold off
axis equal
axis([-1000 1000 -1000 1250 -3000 -900])
axis off vis3d
if idx == 1
camlight
lighting gouraud
end
% rotate the objects
cAxis = gca;

rotate(hJoints,[1 0 0],30)
rotate(CyHandle,[1 0 0],30)
if idx > 1
    rotate(hLines,[1 0 0],30)
end

view(2)
% view(-95+idx,50)
drawnow
if makeMovie
currFrame = getframe;
writeVideo(vidH,currFrame);
end
% pause(0.03)

end





