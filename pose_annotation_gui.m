function varargout = pose_annotation_gui(varargin)
% POSE_ANNOTATION_GUI MATLAB code for pose_annotation_gui.fig
%      POSE_ANNOTATION_GUI, by itself, creates a new POSE_ANNOTATION_GUI or raises the existing
%      singleton*.
%
%      H = POSE_ANNOTATION_GUI returns the handle to a new POSE_ANNOTATION_GUI or the handle to
%      the existing singleton*.
%
%      POSE_ANNOTATION_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in POSE_ANNOTATION_GUI.M with the given input arguments.
%
%      POSE_ANNOTATION_GUI('Property','Value',...) creates a new POSE_ANNOTATION_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before pose_annotation_gui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to pose_annotation_gui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help pose_annotation_gui

% Last Modified by GUIDE v2.5 21-Jan-2017 00:28:28

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @pose_annotation_gui_OpeningFcn, ...
                   'gui_OutputFcn',  @pose_annotation_gui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before pose_annotation_gui is made visible.
function pose_annotation_gui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to pose_annotation_gui (see VARARGIN)

% Choose default command line output for pose_annotation_gui
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes pose_annotation_gui wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = pose_annotation_gui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
sliderpos = get(hObject,'Value');
requiredFrame = round(sliderpos*(handles.totalframes-1))+1;




% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in loadKinectButton.
function loadKinectButton_Callback(hObject, eventdata, handles)
% hObject    handle to loadKinectButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[ir_file, filepath] = uigetfile('*.KIR');
handles.mainFig = figure;
[IRcontainer,TScontainer] = playKIR(fullfile(filepath,ir_file),true);
% implay(IRcontainer);
handles.data = IRcontainer;
handles.timestamps = TScontainer;
% [f,p] = uigetfile({'*.png','Supported images';...
%                  '*.png','Portable Network Graphics (*.png)';...
%                  '*.*','All files (*.*)'});
% x = imread([p f]);
% himage=imshow(x);

figure(handles.mainFig)
himage=imshow(histeq(IRcontainer(:,:,1)));
handles.mainFigAx = gca;
handles.cImageHandle = himage;
handles.frameInd = 1;
handles.totalframes=size(IRcontainer,3);

set(handles.startbutton,'Enable','on') ;
% set(handles.initframe, 'String',num2str(1));
% set(handles.curframe, 'String',num2str(1));
% set(handles.finalframe, 'String',num2str(handles.totalframes));

guidata( hObject, handles);

% --- Executes on button press in startbutton.
% function startbutton_Callback(hObject, eventdata, handles)
% % hObject    handle to startbutton (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% load initial_skeleton
% for i=1:15
%     h(i) = impoint(gca, position{i,1});
%     resume(h(i))
% end
% handles.hPos=h;
% nframes=size(handles.data,3);
% handles.nextPos=cell(nframes,15);
% handles.depth=zeros(nframes,15);
% set(handles.curframe,'string',num2str(handles.frameInd));
% 
% %saving initial and final frame
% guidata( hObject, handles);
% --- Executes on button press in startbutton.
function startbutton_Callback(hObject, eventdata, handles)
% hObject    handle to startbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%initialize joints
load initial_skeleton
handles.circDiam = 10;
figure(handles.mainFig);
% axes(handles.mainFigAx)
%initialize joint-connections
connMat = [1 2; 2 3; 3 4; 4 5; 2 6; 6 7; 7 8; 2 9; 9 10; 10 11; 11 12; 9 13; 13 14; 14 15];
for i=1:14
    posBegin = position{connMat(i,1),1};
    posEnd = position{connMat(i,2),1};
    hConn(i) = line([posBegin(1) posEnd(1)],[posBegin(2) posEnd(2)],'color','k');
end
handles.hConn=hConn;

for i=1:15
%     h(i) = impoint(handles.mainFigAx, position{i,1});
    h(i) = imellipse(handles.mainFigAx,...
        [position{i,1}-[handles.circDiam handles.circDiam]/2 [handles.circDiam handles.circDiam]]);
    h(i).setResizable(false);
    % make left side red, center green
    if any(i==[3 4 5 10 11 12])
        h(i).setColor('red')
    elseif any(i==[1 2 9])
        h(i).setColor('green')
    end
    resume(h(i));
end
handles.hPos=h;

for i=1:15
    % add connection
    addNewPositionCallback(h(i),@(pos)connectJoints(handles,i,connMat,pos));
end
    
nframes=size(handles.data,3);
handles.nextPos=cell(nframes,15);
handles.depth=zeros(nframes,15);
% set(handles.curframe,'string',num2str(handles.frameInd));
set(handles.initframe,'Enable','on') ;
set(handles.finalframe,'Enable','on') ;

% initialize struct to keep track of each frame
handles.framestatus = struct('was_visited',false,'skeleton',[],'occl_depths',[]);

%saving initial and final frame
guidata( hObject, handles);



function connectJoints(handles, jointIndex, connMat, jointPos)
hConn = handles.hConn;
% hPos = handles.hPos;
% search connection begin change
changeIdx = find(any(connMat(:,1) == jointIndex,2));
if ~isempty(changeIdx)
for idx = changeIdx'
    xdat = get(hConn(idx),'XData');
    ydat = get(hConn(idx),'YData');
    set(hConn(idx),'XData',[jointPos(1)+handles.circDiam/2,xdat(2)])
    set(hConn(idx),'YData',[jointPos(2)+handles.circDiam/2,ydat(2)])
end
end
drawnow
% search connection end change
changeIdx = find(any(connMat(:,2) == jointIndex,2));
if ~isempty(changeIdx)
for idx = changeIdx'
    xdat = get(hConn(idx),'XData');
    ydat = get(hConn(idx),'YData');
    set(hConn(idx),'XData',[xdat(1),jointPos(1)+handles.circDiam/2])
    set(hConn(idx),'YData',[ydat(1),jointPos(2)+handles.circDiam/2])
end
end
drawnow
figure(pose_annotation_gui) %same result as: set(0,'CurrentFigure',pose_annotation_gui)

% --- Executes on button press in nextFrame.
function nextFrame_Callback(hObject, eventdata, handles)
% hObject    handle to nextFrame (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if isempty(handles.hPos)==0
    hskeleton=handles.hPos;
    for i=1:15
        tempPos = getPosition(hskeleton(i));
        handles.nextPos{handles.frameInd,i}=tempPos(1:2)+[handles.circDiam handles.circDiam]/2;
    end
end

%Readings offsets from edit boxes-joints
text = get(handles.nose_offset, 'String');depth(handles.frameInd,1)=str2num(text);
text = get(handles.neck_offset, 'String');depth(handles.frameInd,2)=str2num(text);
text = get(handles.Rshoulder_offset ,'String');depth(handles.frameInd,3)=str2num(text);
text = get(handles.Relbow_offset ,'String');depth(handles.frameInd,4)=str2num(text);
text = get(handles.Rwrist_offset ,'String');depth(handles.frameInd,5)=str2num(text);
text = get(handles.Lshoulder_offset ,'String');depth(handles.frameInd,6)=str2num(text);
text = get(handles.Lelbow_offset ,'String');depth(handles.frameInd,7)=str2num(text);
text = get(handles.Lwrist_offset ,'String');depth(handles.frameInd,8)=str2num(text);
text = get(handles.pelvis_offset ,'String');depth(handles.frameInd,9)=str2num(text);
text = get(handles.Rhip_offset ,'String');depth(handles.frameInd,10)=str2num(text);
text = get(handles.Rknee_offset ,'String');depth(handles.frameInd,11)=str2num(text);
text = get(handles.Rfoot_offset ,'String');depth(handles.frameInd,12)=str2num(text);
text = get(handles.Lhip_offset ,'String');depth(handles.frameInd,13)=str2num(text);
text = get(handles.Lknee_offset ,'String');depth(handles.frameInd,14)=str2num(text);
text = get(handles.Lfoot_offset ,'String');depth(handles.frameInd,15)=str2num(text);
handles.depth(handles.frameInd,:)=depth(handles.frameInd,:);

handles.framestatus(handles.frameInd).was_visited = true;
handles.framestatus(handles.frameInd).occl_depths = depth(handles.frameInd,:);
handles.framestatus(handles.frameInd).skeleton = [handles.nextPos{handles.frameInd,:}];

%hskeleton=handles.hPos;
% fileID=handles.fileID;
% [nrows,ncols] = size(handles.nextPos);
% for cols = 1:ncols
%     fprintf(fileID,'%2.3f %2.3f\t%2.1f\n',handles.nextPos{handles.frameInd,cols},depth(cols));
% 
% end

% load next frame
frameInd = handles.frameInd;
frameInd = frameInd+1;
handles.frameInd = frameInd;
%Button is deactivated if reaching the last frame
if frameInd>=handles.nFinal
    set(handles.nextFrame,'Enable','off') 
else
    if frameInd>handles.nInitial
    set(handles.nextFrame,'Enable','on') 
    set(handles.previousFrame,'Enable','on') 
    end
end
    
% display next frame
set(handles.cImageHandle,'CData',histeq(handles.data(:,:,frameInd)));
set(handles.curframe,'string',num2str(frameInd));

% if available, show skeleton of the next frame
if numel(handles.framestatus)>=handles.frameInd % prevent indexing non-existent element
if handles.framestatus(handles.frameInd).was_visited
    hskeleton=handles.hPos;
    for i=1:15
        setPosition(hskeleton(i),handles.nextPos{handles.frameInd,i}-[handles.circDiam handles.circDiam]/2);
    end
end
end

guidata(hObject, handles);


function previousFrame_Callback(hObject, eventdata, handles)
% hObject    handle to previousFrame (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% load prev frame
frameInd = handles.frameInd;

frameInd = frameInd-1;
if frameInd<=handles.nInitial
    set(handles.previousFrame,'Enable','off') 
else
    if frameInd<handles.nFinal
     set(handles.previousFrame,'Enable','on') 
     set(handles.nextFrame,'Enable','on') 
    end
end

set(handles.curframe,'string',num2str(frameInd));
% figure(handles.mainFig);
% himage=imshow(histeq(handles.data(:,:,frameInd)));
set(handles.cImageHandle,'CData',histeq(handles.data(:,:,frameInd)));
% handles.cImageHandle = himage;
handles.frameInd = frameInd;

%initialize joint-connections
connMat = [1 2; 2 3; 3 4; 4 5; 2 6; 6 7; 7 8; 2 9; 9 10; 10 11; 11 12; 9 13; 13 14; 14 15];
for i=1:14
    posBegin = handles.nextPos{frameInd,connMat(i,1)};
    posEnd = handles.nextPos{frameInd,connMat(i,2)};
    set(handles.hConn(i),'XData',[posBegin(1) posEnd(1)],'YData',[posBegin(2) posEnd(2)])
%     hConn(i) = line([posBegin(1) posEnd(1)],[posBegin(2) posEnd(2)],'color','k');
end

if handles.framestatus(handles.frameInd).was_visited
    for i=1:15
        setPosition(handles.hPos(i),[handles.nextPos{handles.frameInd,i}-[handles.circDiam handles.circDiam]/2 [handles.circDiam handles.circDiam]]);
    end
end
% handles.hConn=hConn;
% for i = 1:15
% %     h(i)=impoint(handles.mainFigAx, handles.nextPos{frameInd,i});
%     h(i) = imellipse(handles.mainFigAx,...
%         [handles.nextPos{frameInd,i}-[handles.circDiam handles.circDiam]/2 [handles.circDiam handles.circDiam]]);
%     h(i).setResizable(false);
%      if any(i==[3 4 5 10 11 12])
%         h(i).setColor('red')
%     elseif any(i==[1 2 9])
%         h(i).setColor('green')
%     end
%     resume(h(i));
% end
% handles.hPos=h;

for i=1:15
    % add connection
    addNewPositionCallback(handles.hPos(i),@(pos)connectJoints(handles,i,connMat,pos));
end
%Readings offsets from edit boxes-joints
set(handles.nose_offset, 'String',num2str(handles.depth(handles.frameInd,1)));
set(handles.neck_offset, 'String',num2str(handles.depth(handles.frameInd,2)));
set(handles.Rshoulder_offset ,'String',num2str(handles.depth(handles.frameInd,3)));
set(handles.Relbow_offset ,'String',num2str(handles.depth(handles.frameInd,4)));
set(handles.Rwrist_offset ,'String',num2str(handles.depth(handles.frameInd,5)));
set(handles.Lshoulder_offset ,'String',num2str(handles.depth(handles.frameInd,6)));
set(handles.Lelbow_offset ,'String',num2str(handles.depth(handles.frameInd,7)));
set(handles.Lwrist_offset ,'String',num2str(handles.depth(handles.frameInd,8)));
set(handles.pelvis_offset ,'String',num2str(handles.depth(handles.frameInd,9)));
set(handles.Rhip_offset ,'String',num2str(handles.depth(handles.frameInd,10)));
set(handles.Rknee_offset ,'String',num2str(handles.depth(handles.frameInd,11)));
set(handles.Rfoot_offset ,'String',num2str(handles.depth(handles.frameInd,12)));
set(handles.Lhip_offset ,'String',num2str(handles.depth(handles.frameInd,13)));
set(handles.Lknee_offset ,'String',num2str(handles.depth(handles.frameInd,14)));
set(handles.Lfoot_offset ,'String',num2str(handles.depth(handles.frameInd,15)));

guidata(hObject, handles); 


function initframe_Callback(hObject, eventdata, handles)
% hObject    handle to initframe (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of initframe as text
%        str2double(get(hObject,'String')) returns contents of initframe as a double
text = get(handles.initframe, 'String'); nInitial=str2num(text);

if nInitial >0 && nInitial<handles.totalframes
   handles.frameInd = nInitial;
   handles.nInitial=nInitial;
   set(handles.cImageHandle,'CData',histeq(handles.data(:,:,handles.frameInd)));
   %himage=imshow(histeq(handles.data(:,:,nInitial)));
   %handles.cImageHandle = himage;
   set(handles.nextFrame,'Enable','on') ;
   set(handles.curframe,'string',num2str(handles.frameInd));
end

guidata( hObject, handles);

function finalframe_Callback(hObject, eventdata, handles)
% hObject    handle to finalframe (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of finalframe as text
%        str2double(get(hObject,'String')) returns contents of finalframe as a double

text = get(handles.finalframe, 'String'); nFinal=str2num(text);
if nFinal >handles.nInitial && nFinal<handles.totalframes
   handles.nFinal=nFinal;
end


guidata( hObject, handles);

% --- Executes on button press in savefile.
function savefile_Callback(hObject, eventdata, handles)
% hObject    handle to savefile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[filename1,filepath1]=uiputfile({'*.txt*','Text Files'},...
    'Save New File');
fileID=fopen(fullfile(filepath1,filename1),'w');
[nrows,ncols] = size(handles.nextPos);
for iFr = handles.nInitial:handles.frameInd
    fprintf(fileID,'%2.0f\n',iFr);
    fprintf(fileID,'%6.10f\n',handles.timestamps(iFr));
    for cols = 1:ncols
        fprintf(fileID,'%2.3f %2.3f %2.3f\n',handles.nextPos{iFr,cols},handles.depth(iFr,cols));
    end
end
fclose(fileID);


function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function startbutton_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton7.
function pushbutton7_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton8.
function pushbutton8_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton9.
function pushbutton9_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton10.
function pushbutton10_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton11.
function pushbutton11_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton12.
function pushbutton12_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton13.
function pushbutton13_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton14.
function pushbutton14_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton15.
function pushbutton15_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton16.
function pushbutton16_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton17.
function pushbutton17_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton17 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton18.
function pushbutton18_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton18 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton19.
function pushbutton19_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton19 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




function Lshoulder_offset_Callback(hObject, eventdata, handles)
% hObject    handle to Lshoulder_offset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Lshoulder_offset as text
%        str2double(get(hObject,'String')) returns contents of Lshoulder_offset as a double


% --- Executes during object creation, after setting all properties.
function Lshoulder_offset_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Lshoulder_offset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function neck_offset_Callback(hObject, eventdata, handles)
% hObject    handle to neck_offset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of neck_offset as text
%        str2double(get(hObject,'String')) returns contents of neck_offset as a double


% --- Executes during object creation, after setting all properties.
function neck_offset_CreateFcn(hObject, eventdata, handles)
% hObject    handle to neck_offset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Rshoulder_offset_Callback(hObject, eventdata, handles)
% hObject    handle to Rshoulder_offset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Rshoulder_offset as text
%        str2double(get(hObject,'String')) returns contents of Rshoulder_offset as a double


% --- Executes during object creation, after setting all properties.
function Rshoulder_offset_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Rshoulder_offset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Relbow_offset_Callback(hObject, eventdata, handles)
% hObject    handle to Relbow_offset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Relbow_offset as text
%        str2double(get(hObject,'String')) returns contents of Relbow_offset as a double


% --- Executes during object creation, after setting all properties.
function Relbow_offset_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Relbow_offset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Lelbow_offset_Callback(hObject, eventdata, handles)
% hObject    handle to Lelbow_offset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Lelbow_offset as text
%        str2double(get(hObject,'String')) returns contents of Lelbow_offset as a double


% --- Executes during object creation, after setting all properties.
function Lelbow_offset_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Lelbow_offset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Lwrist_offset_Callback(hObject, eventdata, handles)
% hObject    handle to Lwrist_offset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Lwrist_offset as text
%        str2double(get(hObject,'String')) returns contents of Lwrist_offset as a double


% --- Executes during object creation, after setting all properties.
function Lwrist_offset_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Lwrist_offset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Rwrist_offset_Callback(hObject, eventdata, handles)
% hObject    handle to Rwrist_offset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Rwrist_offset as text
%        str2double(get(hObject,'String')) returns contents of Rwrist_offset as a double


% --- Executes during object creation, after setting all properties.
function Rwrist_offset_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Rwrist_offset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Lhip_offset_Callback(hObject, eventdata, handles)
% hObject    handle to Lhip_offset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Lhip_offset as text
%        str2double(get(hObject,'String')) returns contents of Lhip_offset as a double


% --- Executes during object creation, after setting all properties.
function Lhip_offset_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Lhip_offset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Rhip_offset_Callback(hObject, eventdata, handles)
% hObject    handle to Rhip_offset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Rhip_offset as text
%        str2double(get(hObject,'String')) returns contents of Rhip_offset as a double


% --- Executes during object creation, after setting all properties.
function Rhip_offset_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Rhip_offset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Rknee_offset_Callback(hObject, eventdata, handles)
% hObject    handle to Rknee_offset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Rknee_offset as text
%        str2double(get(hObject,'String')) returns contents of Rknee_offset as a double


% --- Executes during object creation, after setting all properties.
function Rknee_offset_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Rknee_offset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Lknee_offset_Callback(hObject, eventdata, handles)
% hObject    handle to Lknee_offset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Lknee_offset as text
%        str2double(get(hObject,'String')) returns contents of Lknee_offset as a double


% --- Executes during object creation, after setting all properties.
function Lknee_offset_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Lknee_offset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Rfoot_offset_Callback(hObject, eventdata, handles)
% hObject    handle to Rfoot_offset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Rfoot_offset as text
%        str2double(get(hObject,'String')) returns contents of Rfoot_offset as a double


% --- Executes during object creation, after setting all properties.
function Rfoot_offset_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Rfoot_offset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Lfoot_offset_Callback(hObject, eventdata, handles)
% hObject    handle to Lfoot_offset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Lfoot_offset as text
%        str2double(get(hObject,'String')) returns contents of Lfoot_offset as a double


% --- Executes during object creation, after setting all properties.
function Lfoot_offset_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Lfoot_offset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function pelvis_offset_Callback(hObject, eventdata, handles)
% hObject    handle to pelvis_offset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of pelvis_offset as text
%        str2double(get(hObject,'String')) returns contents of pelvis_offset as a double


% --- Executes during object creation, after setting all properties.
function pelvis_offset_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pelvis_offset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end





% --- Executes during object creation, after setting all properties.
function initframe_CreateFcn(hObject, eventdata, handles)
% hObject    handle to initframe (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end





% --- Executes during object creation, after setting all properties.
function finalframe_CreateFcn(hObject, eventdata, handles)
% hObject    handle to finalframe (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function curframe_Callback(hObject, eventdata, handles)
% hObject    handle to curframe (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of curframe as text
%        str2double(get(hObject,'String')) returns contents of curframe as a double


% --- Executes during object creation, after setting all properties.
function curframe_CreateFcn(hObject, eventdata, handles)
% hObject    handle to curframe (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on key press with focus on figure1 or any of its controls.
function figure1_WindowKeyPressFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.FIGURE)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)

switch eventdata.Character
    case 'w'
        nextFrame_Callback(handles.nextFrame, eventdata, handles)
    case 'q'
        previousFrame_Callback(handles.previousFrame, eventdata, handles)
end